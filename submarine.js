
const cvs = document.getElementById("canvas");
const ctx = cvs.getContext("2d");

// load images
const sm = new Image();
const bg = new Image();
const shark = new Image();
const bumb = new Image();
const ball = new Image();
const smleft = new Image();

bg.src = "images/bg.png";
sm.src = "images/sm.png";
shark.src = "images/shark.png";
bumb.src = "images/bumb.png";
ball.src = "images/ball.png";
smleft.src = "images/smleft.png";

// some variables
let sX = 150;
let sY = 210;

//score
const score = 0;

// audio files
const scor = new Audio();
scor.src = "sounds/score.mp3";

const sharkpos = [];
sharkpos[0] = {
    x : cvs.width,
    y : 300
};

let subR = true;

const balpos = [];
balpos[0] = {
    x : sX,
    y : sY,
    direction: subR
};

let fire = false;


// arrow key presses
document.addEventListener('keydown', function(e) {
    switch (e.keyCode) {
        case 37:
            sX -= 10;
            subR = false;         
            break;
        case 38:
            sY -= 10;
            break;
        case 39:
            sX += 10;
            subR = true;
            break;
        case 40:
            sY += 10;
            break;
        case 32:
            fire = true;
            balpos[0].direction = subR;
            balpos[0].x = sX;
            balpos[0].y = sY;
            break;
    }
});

function draw() {

    ctx.fillStyle = "#000";
    ctx.font = "20px Verdana";
    ctx.fillText("Score : " + score, 10, cvs.height-90);

    ctx.drawImage(bg, 0, 0);

    if (subR){
        ctx.drawImage(sm, sX, sY);
    }else{
        ctx.drawImage(smleft, sX, sY);
    }
    
    if (fire) {
        ctx.drawImage(ball, balpos[0].x , balpos[0].y );
        
        if (balpos[0].direction){
            balpos[0].x += 10;
            // if (balpos[0].x < ctx.width ) continue;
        }else {
            balpos[0].x -= 10;
        }
    }

    for(var i = 0; i < sharkpos.length; i++) {

        ctx.drawImage(shark, sharkpos[i].x, sharkpos[i].y);

        sharkpos[i].x--;

        if(sharkpos[i].x == 1200){
            sharkpos.push({
                x : cvs.width,
                y : Math.floor(Math.random()*500)+150    // y : Math.floor(Math.random()*shark.width)+150
            });
        }
        // if( sX + sm.width >= sharkpos[i].x &&
        //     sX <= sharkpos[i].x + sm.width &&
        //     (sY <= sharkpos[i].y + sm.height || sY+sm.height >= sharkpos[i].y + constant) ||
        //     sY + sm.height >=  cvs.height - cvs.height) {
        //         location.reload();
        // }
    }

    requestAnimationFrame(draw);
}   

draw();